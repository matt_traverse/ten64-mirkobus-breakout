# TEN64 mikroBUS adapter

![Photo of adapter board and Mikro Temp and Hub 14](Mikro_14.jpg)
![Photo of adapter board installed in Ten64](Mikro_16.jpg)

This is a simple adapter/interposer which allows
[mikroBUS](https://www.mikroe.com/mikrobus) plug-in
boards ("clicks") to be used with the Ten64 via it's [GPIO/control header](https://ten64doc.traverse.com.au/hardware/control-header/).

**Warning:** The interface with the host board is not "keyed" and
shrouded (unlike an IDC ribbon cable) so it is very easy to misalign
the connector.

A good way to be sure is to ensure GND on the mikroBUS is connected/short
with GND on the main board.

The interface with the board connector is effectively "friction-fit" so
no guarantees are made about it's ability to withstand vibration and
movement during transit.

**This board is not a Traverse Product and is not supported for
production use for reasons stated above**

## Compatible functions

This adapter board is best suited for clicks that only use:

* I2C
* UART
* GPIO

SPI can be used with [GPIO bitbanging](https://elixir.bootlin.com/linux/v5.16-rc5/source/Documentation/devicetree/bindings/spi/spi-gpio.yaml)
(tested with Temp & Hum 16) but is otherwise not recommended.

Ten64 does not have any analogue or PWM functions on it's GPIOs.

mikroBUS pins `CS` and `INT` can be changed between 4-wire UART and GPIO
depending on designed function with a jumper.

## Tested boards

* [Temp and Hum 14](https://www.mikroe.com/temphum-14-click)
* [Temp and Hum 16](https://www.mikroe.com/temphum-16-click)

## Schematic

![Schematic](Schematic.png)

## Bill of Materials

| Designator      | Description                      | Manufacturer     | Manufacturer Part Number |
| --------------- | -------------------------------- | ---------------- | ------------------------ |
| P1              | CONN RCPT 26POS 0.05 GOLD SMD    | Samtec           | FLE-113-01-G-DV-A        |
| P2-1,P2-2       | CONN RCPT 8POS 0.1 GOLD PCB      | Amphenol FCI     | 75915-308GLF             |
| P3,P4           | PIN HEADER 2.54MM PITCH STR,1X3P | Amphenol ICC     | G800W305018EU            |
| P3-JUMP,P4-JUMP | JUMPER W/TEST PNT 1X2PINS 2.54MM | Wurth Elektronik | 60900213421              |

## Build your own

I have [shared](https://www.pcbway.com/project/shareproject/mikroBUS_adapter_board_for_Traverse_Ten64_866de150.html) the project on pcbway.com

## See also

* [GPIO adapter board](https://gitlab.com/matt_traverse/ten64-gpio-breakout)
